﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Globalization;
using System.Data.SqlClient;

namespace Importazioni_NE
{
    class Program
    {

        XmlDocument docXml = new XmlDocument();
        SAPbobsCOM.Company oCompany;
        private SAPbobsCOM.Items oArt;
        private SAPbobsCOM.Documents oDoc;
        private SAPbobsCOM.JournalEntries oPN;
        private SAPbobsCOM.Recordset oRecordset;
        private SAPbobsCOM.Recordset oRecordset2;
        public SAPbobsCOM.BusinessPartners oBP;

        string fileLogDB;

        static void Main(string[] args)
        {
            var prog = new Program();

            prog.docXml.Load("parametriDB.xml");

            Log("  Avvio Importazione");

            prog.ConnessioneSQL(prog.docXml);
            prog.ConnessioneSAP(prog.docXml);
            prog.Articoli();
            prog.FattVendita();

            prog.DisconessioneSAP();
            prog.DisconnessioneSQL();

        }

        static void Log(string logMessage)
        {
            DateTime dt = new DateTime();
            dt = DateTime.Now;
            string logFile = "c:/temp/Log_Importazioni" + "_" + dt.ToString("yyyyMMdd") + ".txt";

            using (StreamWriter w = File.AppendText(logFile))
            {

                w.WriteLine(logMessage);

            }
            Console.WriteLine(logMessage);
        }

        public void ConnessioneSQL(XmlDocument docXml)
        {
            string NomeDB;
            string Server;
            string User;
            string Psw;
            XmlNodeList elemlist = docXml.GetElementsByTagName("SQL");
            foreach (XmlNode node in elemlist)
            {
                Server = node.Attributes["Server"].Value;
                NomeDB = node.Attributes["NomeDB"].Value;
                User = node.Attributes["DbUser"].Value;
                Psw = node.Attributes["DbPsw"].Value;
                string connetionString;
                SqlConnection conSQL;
                connetionString = @"Data Source=" + Server + ";Initial Catalog=" + NomeDB + ";User ID=" + User + ";Password=" + Psw + ";";
                conSQL = new SqlConnection(connetionString);
                conSQL.Open();
                Log("Connessione a database SQL: OK");
            }
        }

        public void ConnessioneSAP(XmlDocument docXml)
        {
            oCompany = new SAPbobsCOM.Company();
            XmlNodeList elemlist = docXml.GetElementsByTagName("SAP");
            foreach (XmlNode node in elemlist)
            {
                oCompany.Server = node.Attributes["Server"].Value;
                oCompany.DbUserName = node.Attributes["DbUsername"].Value; //se errore commentare username e password DB
                oCompany.DbPassword = node.Attributes["DbPassword"].Value;
                oCompany.CompanyDB = node.Attributes["CompanyDB"].Value;
                oCompany.UserName = node.Attributes["Username"].Value;
                oCompany.Password = node.Attributes["Password"].Value;
            }
            oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014;
            oCompany.UseTrusted = false;

            int errCode = oCompany.Connect();
            if (errCode != 0)
            {
                string err;
                int code;
                oCompany.GetLastError(out code, out err);
                Log("   Connessione database non riuscita,  errore: " + code + " - " + err);
            }
            else
            {
                Log("   Connessione al database SAP: OK ");
            }
        }

        public void Articoli ()
        {
            System.Data.DataTable oTab = new System.Data.DataTable();
            SqlDataAdapter oAdp = new SqlDataAdapter();

            //Query SQL
            string Articoli = "SELCT * FROM dbo.v_articoli";
            SqlCommand command = new SqlCommand(Articoli);
            SqlDataReader reader = command.ExecuteReader();

            foreach (System.Data.DataRow myRS in oTab.Rows)
            {
                string Id_Articolo = reader["ID_Articolo"].ToString();
                string CodiceArticolo = reader["Codice_Articolo"].ToString();
                string Descrizione_Articolo = reader["Descrizione_Articolo"].ToString();

                oCompany.StartTransaction();

                oArt.ItemCode = CodiceArticolo;
                oArt.ItemName = Descrizione_Articolo;
                oArt.ForeignName = Id_Articolo;

                oArt.Add();

                int res = oArt.Add();
                if (res == 0)
                {
                    oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit); //Convalida l'operazione 
                                                                                //oRecordset.DoQuery("");

                    Log("   " + "Articolo" + "    n: " + CodiceArticolo + "   Inserita correttamente");
                }
                else
                {
                    Log("ERRORE in inserimento Articolo     n: " + CodiceArticolo + oCompany.GetLastErrorCode() + " - " + oCompany.GetLastErrorDescription());
                }
            }

        }

        public void FattVendita() //Inserimento Testata fatture vendita
        {

            oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            System.Data.DataTable oTab = new System.Data.DataTable();
            SqlDataAdapter oAdp = new SqlDataAdapter();

            //Query SQL
            string Testata = "SELCT * FROM Sap.Testata";
            SqlCommand command = new SqlCommand(Testata);
            SqlDataReader reader = command.ExecuteReader();

            foreach (System.Data.DataRow myRS in oTab.Rows)
            {
                
                    //dichiarazioni variabili 
                    string DocNumTestata = reader["Numero_Documento"].ToString();
                    string dataSQL = reader["Data_Documento"].ToString();
                    string dataivaSQL = reader["Data_Documento"].ToString();
                    string vatdateSQL = reader["VATDATE"].ToString();
                    DateTime docDate;
                    DateTime taxDate;
                    DateTime vatDate;
                    string DocType = reader["DOCTYPE"].ToString();
                    string PayGroupCodes = reader["PAYMENTGROUPCODE"].ToString();
                    string Series = reader["SERIES"].ToString();
                    string TipoDoc = reader["DOCTYPE1"].ToString();

                    //Conversioni variabili
                    docDate = Convert.ToDateTime("dataSQL");
                    taxDate = Convert.ToDateTime("dataivaSQL");
                    vatDate = Convert.ToDateTime("vatdateSQL");
                    int iSeries = Int32.Parse(Series);

                if (checkDoc(TipoDoc, DocNumTestata))
                {
                    //Inizio Inserimento documento
                    oCompany.StartTransaction();

                    SAPbobsCOM.BoObjectTypes tipoDocumento;
                    if (String.Equals(TipoDoc, "F")) //controllo tipo doc: Fattura o Nota Credito
                    {
                        tipoDocumento = SAPbobsCOM.BoObjectTypes.oInvoices;
                    }
                    else
                    {
                        tipoDocumento = SAPbobsCOM.BoObjectTypes.oCreditNotes;
                    }
                    oDoc = oCompany.GetBusinessObject(tipoDocumento);
                    oDoc.CardCode = reader["CARDCODE"].ToString();

                    if (String.Equals(DocType, "S")) //controllo se documento servizi o articoli
                    {
                        oDoc.DocType = SAPbobsCOM.BoDocumentTypes.dDocument_Service;
                    }
                    else
                    {
                        oDoc.DocType = SAPbobsCOM.BoDocumentTypes.dDocument_Items;
                    }
                    //inizio inserimento dati documento
                    oDoc.DocDate = docDate;
                    oDoc.TaxDate = taxDate;
                    oDoc.NumAtCard = reader["NUMATCARD"].ToString();
                    oDoc.DocCurrency = reader["DOCCURRENCY"].ToString();
                    //oDoc.DocRate = 0; //Cambio SQL
                    oDoc.PaymentGroupCode = 0000;  //Codice condizione pagamento
                    oDoc.Series = iSeries; //Serie 
                                           //oDoc.PaymentMethod = reader["PAYMENTMETOD"].ToString();
                    oDoc.VatDate = vatDate;

                    RigheFattVendita(oDoc, DocNumTestata); // void inserimento fatture

                    //Aggiunta Documento ed esito
                    int res = oDoc.Add();
                    if (res == 0)
                    {
                        oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit); //Convalida l'operazione 
                        //oRecordset.DoQuery("");

                        Log("   " + "Fattura Vendita" + "    n: " + DocNumTestata + "   Inserita correttamente");
                    }
                    else
                    {
                        Log("ERRORE in inserimento Fattura di Vendita     n: " + DocNumTestata + oCompany.GetLastErrorCode() + " - " + oCompany.GetLastErrorDescription());
                    }
                }
                else
                {
                 
                }
               
            }
        }

        public void RigheFattVendita(SAPbobsCOM.Documents oDoc, string DocNumTestata)
        {
            //Query SQL
            string Righe = "SELCT * FROM Sap.Righe";
            SqlCommand command = new SqlCommand(Righe);
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                
                //Dichiarazione Variabili
                string DocNumRighe = reader["DOCNUM"].ToString();
                string LineTotal = reader["LINETOTAL"].ToString();
                string LineNum = reader["LINENUM"].ToString();
                int LineNumS;

                //Conversioni Variabili
                int iLineTotal = Int32.Parse(LineTotal);
                int iLineNum = Int32.Parse(LineNum);

                if (String.Equals(DocNumTestata, DocNumRighe))
                {

                    oDoc.Lines.ItemDescription = reader["ITEMNAME"].ToString();
                    oDoc.Lines.AccountCode = reader["ACCOUNTCODE"].ToString();
                    oDoc.Lines.VatGroup = "CodIVA";
                    oDoc.Lines.LineTotal = 000;
                    //oDoc.Lines.ProjectCode = "123";

                    LineNumS = oDoc.Lines.LineNum;
                    if (String.Equals(LineNumS, iLineNum))
                    {
                        oDoc.Lines.Add();
                        Log("   " + "Riga Fattura Vendita" + "    n: " + DocNumRighe + "   Inserita correttamente");
                    }
                    else
                    {
                        Log("   " + "ERRORE in Inserimento Riga Fattura Vendita" + "    n: " + DocNumRighe );
                    }
                }
                else
                {

                }
            }
        }

        public bool checkDoc(string TipoDoc, string DocNum)
        {
            oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordset2 = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            string Count = "";
            string DocNumSAP = "";

            //Se Fattura
            if(String.Equals(TipoDoc, "F"))
            {
                oRecordset.DoQuery("SELECT DISTINCT(*) FROM OINV WHERE \"NumAtCard\" = '" + DocNum + "'");
                Count = oRecordset.Fields.Item(0).Value.ToString();

                if (Count != "0")
                {
                    oRecordset2.DoQuery("SELECT \"DocNum\" FROM OINV WHERE \"NumAtCard\" = '" + DocNum + "'");
                    DocNumSAP = oRecordset2.Fields.Item(0).Value.ToString();
                    Log("   " + "ERRORE, Documento già esistente in SAP con codice:" + DocNumSAP);
                    return false;
                }
            }
            //Se nota di credito
            else if (String.Equals(TipoDoc, "N"))
            {
                oRecordset.DoQuery("SELECT DISTINCT(*) FROM ORIN WHERE \"NumAtCard\" = '" + DocNum + "'");
                Count = oRecordset.Fields.Item(0).Value.ToString();

                if (Count != "0")
                {
                    oRecordset2.DoQuery("SELECT \"DocNum\" FROM ORIN WHERE \"NumAtCard\" = '" + DocNum + "'");
                    DocNumSAP = oRecordset2.Fields.Item(0).Value.ToString();
                    Log("   " + "ERRORE, Documento già esistente in SAP con codice:" + DocNumSAP);
                    return false;
                }
            }
            return true;

        }

        public void DisconessioneSAP()
        {
            oCompany.Disconnect();
            oCompany = null;
            Log("   Disconnessione dal database SAP: OK");
        }

        public void DisconnessioneSQL()
        {
            string connetionString;
            SqlConnection conSQL;
            connetionString = @"Data Source=WIN-50GP30FGO75;Initial Catalog=Demodb;User ID=sa;Password=demol23";
            conSQL = new SqlConnection(connetionString);
            conSQL.Close();
            Log("Disconnessione dal database SQL: OK");

        }

    }
}